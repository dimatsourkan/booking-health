import { Component, OnInit } from '@angular/core';
import { FilesService } from '../../core/entities/files/files.service';
import { FileManagerService } from '../../../../projects/file-manager/src/lib/file-manager.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: [ './home.component.scss' ]
})
export class HomeComponent implements OnInit {
  constructor(
    private filesService: FilesService,
    private fileManagerService: FileManagerService
  ) {
  }

  ngOnInit() {
    this.getFiles();
  }

  getFiles() {
    this.filesService.getList().subscribe(res => {
      this.fileManagerService.setList(res.files);
    });
  }
}
