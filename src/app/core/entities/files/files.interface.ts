import { File } from '../../../../../projects/file-manager/src/lib/models/file.interface';

export interface FileResult {
  files: File[];
}
