import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { filesList } from './files.list';
import { FileResult } from './files.interface';

@Injectable({
  providedIn: 'root'
})
export class FilesService {

  getList(): Observable<FileResult> {
    return of(filesList);
  }
}
