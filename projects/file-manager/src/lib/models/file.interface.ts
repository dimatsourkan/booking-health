export interface File {
  name: string;
  dateModified: string;
  dateCreated: string;
  hasChild?: boolean;
  isFile?: boolean;
  size ?: number;
  files?: File[];
  sub?: File;
}
