import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { File } from './models/file.interface';

@Injectable()
export class FileManagerService {

  private list = new BehaviorSubject<File[]>(null);
  public list$ = this.list.asObservable();

  private currentFolder = new BehaviorSubject<File>(null);
  public currentFolder$ = this.currentFolder.asObservable();

  setCurrentFolder(file: File) {
    this.currentFolder.next(file);
  }

  setList(files: File[]) {
    this.list.next(files);
  }

}
