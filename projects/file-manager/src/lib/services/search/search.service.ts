import { Injectable } from '@angular/core';
import { FileManagerService } from '../../file-manager.service';
import { File } from '../../models/file.interface';

@Injectable()
export class SearchService {
  private globalFiles: File[] = [];

  private findedFiles: File[] = [];

  constructor( private fileManagerService: FileManagerService ) {
    this.fileManagerService.list$.subscribe(list => {
      this.globalFiles = list;
    });
  }

  /**
   * Emit search
   * @param search
   */
  searchChange( search: string ) {
    this.findedFiles = [];
    this.findInList(this.globalFiles, search);
    this.fileManagerService.setCurrentFolder({ files: this.findedFiles } as File);
  }

  /**
   * Recursive file search
   * @param files
   * @param search
   */
  findInList( files: File[] = [], search: string) {
    files.forEach(( file ) => {
      if (file.isFile && file.name && file.name.includes(search)) {
        this.findedFiles.push(file);
      }
      if (file.files && file.files.length) {
        this.findInList(file.files, search);
      }
      if (file.sub) {
        this.findInList(file.sub.files, search);
      }
    });
  }
}
