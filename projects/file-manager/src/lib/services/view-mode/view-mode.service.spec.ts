import { TestBed } from '@angular/core/testing';

import { ViewModeService } from './view-mode.service';

describe('ViewModeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ViewModeService = TestBed.get(ViewModeService);
    expect(service).toBeTruthy();
  });
});
