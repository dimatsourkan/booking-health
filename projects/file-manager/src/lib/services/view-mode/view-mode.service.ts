import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ViewMode } from './view-mode';

@Injectable()
export class ViewModeService {
  private viewMode = new BehaviorSubject(ViewMode.module);

  public viewMode$ = this.viewMode.asObservable();

  changeViewMode( mode: ViewMode ) {
    this.viewMode.next(mode);
  }
}
