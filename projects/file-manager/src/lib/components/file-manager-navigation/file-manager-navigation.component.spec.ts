import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileManagerNavigationComponent } from './file-manager-navigation.component';

describe('FileManagerNavigationComponent', () => {
  let component: FileManagerNavigationComponent;
  let fixture: ComponentFixture<FileManagerNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileManagerNavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileManagerNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
