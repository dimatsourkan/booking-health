import { Component, OnInit } from '@angular/core';
import { File } from '../../models/file.interface';
import { FileManagerService } from '../../file-manager.service';

@Component({
  selector: 'lib-file-manager-navigation',
  templateUrl: './file-manager-navigation.component.html',
  styleUrls: [ './file-manager-navigation.component.scss' ]
})
export class FileManagerNavigationComponent implements OnInit {
  files$ = this.fileManagerService.list$;

  openFolders: { [ name: string ]: boolean } = {};

  constructor( private fileManagerService: FileManagerService ) {
  }

  ngOnInit() {
  }

  isOpen( name: string ) {
    return this.openFolders[ name ];
  }

  toggleFolder( name: string ) {
    this.openFolders[ name ] = !this.openFolders[ name ];
  }

  setCurrentFolder( file: File ) {
    if (!file.isFile) {
      this.fileManagerService.setCurrentFolder(file);
      this.toggleFolder(file.name);
    }
  }
}
