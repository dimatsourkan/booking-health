import { Component, OnInit } from '@angular/core';
import { FileManagerService } from '../../file-manager.service';
import { ViewModeService } from '../../services/view-mode/view-mode.service';
import { ViewMode } from '../../services/view-mode/view-mode';
import { File } from '../../models/file.interface';

@Component({
  selector: 'lib-file-manager-content',
  templateUrl: './file-manager-content.component.html',
  styleUrls: ['./file-manager-content.component.scss']
})
export class FileManagerContentComponent implements OnInit {

  readonly viewModeType = ViewMode;
  readonly folder$ = this.fileManagerService.currentFolder$;
  readonly viewMode$ = this.viewModeService.viewMode$;

  constructor(
    private fileManagerService: FileManagerService,
    private viewModeService: ViewModeService,
  ) { }

  ngOnInit() {
  }

  setCurrentFolder( file: File ) {
    if (!file.isFile) {
      this.fileManagerService.setCurrentFolder(file);
    }
  }

}
