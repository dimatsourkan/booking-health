import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileManagerContentComponent } from './file-manager-content.component';

describe('FileManagerContentComponent', () => {
  let component: FileManagerContentComponent;
  let fixture: ComponentFixture<FileManagerContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileManagerContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileManagerContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
