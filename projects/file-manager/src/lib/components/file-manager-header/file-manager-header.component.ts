import { Component, OnInit } from '@angular/core';
import { ViewModeService } from '../../services/view-mode/view-mode.service';
import { ViewMode } from '../../services/view-mode/view-mode';
import { SearchService } from '../../services/search/search.service';

@Component({
  selector: 'lib-file-manager-header',
  templateUrl: './file-manager-header.component.html',
  styleUrls: [ './file-manager-header.component.scss' ]
})
export class FileManagerHeaderComponent implements OnInit {
  readonly viewModeType = ViewMode;

  readonly viewMode$ = this.viewModeService.viewMode$;

  constructor(
    private viewModeService: ViewModeService,
    private searchService: SearchService
  ) {

  }

  changeMode( mode: ViewMode ) {
    this.viewModeService.changeViewMode(mode);
  }

  search(search: string) {
    this.searchService.searchChange(search);
  }

  ngOnInit() {
  }
}
