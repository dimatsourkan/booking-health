import { NgModule } from '@angular/core';
import { FileManagerComponent } from './file-manager.component';
import { FileManagerHeaderComponent } from './components/file-manager-header/file-manager-header.component';
import { FileManagerContentComponent } from './components/file-manager-content/file-manager-content.component';
import { CommonModule } from '@angular/common';
import { FileManagerBreadcrumbsComponent } from './components/file-manager-breadcrumbs/file-manager-breadcrumbs.component';
import { FileManagerNavigationComponent } from './components/file-manager-navigation/file-manager-navigation.component';
import { ViewModeService } from './services/view-mode/view-mode.service';
import { FileManagerService } from './file-manager.service';
import { SearchService } from './services/search/search.service';

@NgModule({
  declarations: [
    FileManagerComponent,
    FileManagerHeaderComponent,
    FileManagerContentComponent,
    FileManagerBreadcrumbsComponent,
    FileManagerNavigationComponent
  ],
  exports: [
    FileManagerComponent,
    FileManagerHeaderComponent,
    FileManagerContentComponent,
    FileManagerBreadcrumbsComponent,
    FileManagerNavigationComponent
  ],
  imports: [
    CommonModule
  ],
  providers: [
    FileManagerService,
    ViewModeService,
    SearchService
  ]
})
export class FileManagerModule {
}
